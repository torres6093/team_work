﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace team_work
{
    public partial class MoveForm : Form
    {
        public string CityChosen;
        public MoveForm()
        {
            InitializeComponent();
            
        }

        public void Init(string MyCity)
        {
            foreach (var city in (Application.OpenForms[0] as MainForm).game.CityNamesList)
            {
                if(city != MyCity)
                    lbCities.Items.Add(city);
            }
        }
        private void btnOK_Click(object sender, EventArgs e)
        {
            var r = lbCities.SelectedItem;
            if (r != null)
            {
                CityChosen = r as string;
                DialogResult = DialogResult.OK;
            }
        }
    }
}
