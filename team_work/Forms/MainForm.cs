﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Windows.Forms;
using team_work.Classes;
using team_work.Interface;

namespace team_work
{
    public partial class MainForm : Form
    {
        public Game game = new Game(1000000, DateTime.Today.AddHours(DateTime.Now.Hour));
        public MainForm()
        {
            InitializeComponent();
            lblDate.Text = "Сегодня: " + DateTime.Now.ToLongDateString();
            lblMoney.Text = "Мои деньги: $1000000";
            UpdateInfo();
            MainTimer.Start();
            game.RegenerateConrtracts(10);
            RegenerateConrtracts();

            for (int i = 0; i < 10; i++)
            {
                flpMarket.Controls.Add(new AirplanePanel(game.MarketAirplanes[i], false));
            }

        }

        // Обновить полёты на форме
        public void RegenerateConrtracts()
        {
            flpFlights.Controls.Clear();
            foreach (var i in game.Flights)
            {
                flpFlights.Controls.Add(new FlightPanel(i));
            }
        }

        // Перенести самолёт к нам
        public void ToMy(AirplanePanel panel)
        {
            flpMarket.Controls.Remove(panel);
            flpMyAirplanes.Controls.Add(panel);
            UpdateInfo();
        }

        // Перенести самолёт на рынок
        public void ToMarket(AirplanePanel panel)
        {
            flpMyAirplanes.Controls.Remove(panel);
            flpMarket.Controls.Add(panel);
            UpdateInfo();
        }

        // Обновить данные по самолётам
        public void UpdateAirplanesInfo()
        {
            foreach (var i in flpMyAirplanes.Controls)
            {
                (i as AirplanePanel).UpdateInfo();
            }
        }

        private string DaysLabel(int d)
        {
            if (d == 1) return " день";
            if (d < 5) return " дня";
            return " дней";
        }
        // Обновить данные по деньгам и времени
        public void UpdateInfo()
        {
            lblDate.Text = "Сегодня: " + game.Date.ToLongDateString() + " " + game.Date.ToLongTimeString();
            lblMoney.Text = "Мои деньги: $" + game.Money.ToString();
            if (game.Debt)
            {
                lblMoney.BackColor = System.Drawing.Color.OrangeRed;
                int d = (game.Date - game.DebtStart).Days;
                lblMoney.Text += $" (долг {3 - d}{DaysLabel(3 - d)})";
                if (d == 3)
                {
                    game.Wait();
                    MessageBox.Show("Вы проиграли");
                    Close();
                }
            }
            else
            {
                lblMoney.BackColor = System.Drawing.Color.Transparent;
            }
            lblFuelCost.Text = $"Цена на топливо: {game.FuelPrice} ($/км)";
        }

        // Обновить данные по контрактам
        public void UpdateContracts(DateTime now)
        {
            for (int i = 0; i < flpContracts.Controls.Count; i++)
            {
                (flpContracts.Controls[i] as ContractPanel).UpdateInfo(now);
            }
        }

        // Перенести контракт к нам
        public void AddContract(FlightPanel panel, Flight contract)
        {
            flpFlights.Controls.Remove(panel);
            flpContracts.Controls.Add(new ContractPanel(contract, false));
        }
        // Отменить взятый контракт к нам
        public void CancelContract(ContractPanel panel, Flight flight)
        {
            flpContracts.Controls.Remove(panel);
            flpFlights.Controls.Add(new FlightPanel(flight));
        }
        public void ArchiveContract(ContractPanel panel)
        {
            flpContracts.Controls.Remove(panel);
            flpHistory.Controls.Add(panel);
        }

        private void MainTimer_Tick(object sender, EventArgs e)
        {
            game.Tick();
        }

        private void lblSpeed_Click(object sender, EventArgs e)
        {
            int newInterval = 4000;
            switch (MainTimer.Interval)
            {
                case 4000:
                {
                    newInterval = 2000;
                    lblSpeed.Text = "Скорость: 2X";
                    break;
                }
                case 2000:
                {
                    newInterval = 1000;
                    lblSpeed.Text = "Скорость: 4X";
                    break;
                }
                case 1000:
                {
                    newInterval = 4000;
                    lblSpeed.Text = "Скорость: 1X";
                    break;
                }
                default:
                    break;
            }
            MainTimer.Interval = newInterval;
        }

        private void SaveGame()
        {
            MainTimer.Stop();
            using (var streamWriter = new FileStream("save.dat", FileMode.OpenOrCreate, FileAccess.Write))
            {
                BinaryFormatter formatter = new BinaryFormatter();
                formatter.Serialize(streamWriter, game);
            }
            MainTimer.Start();
        }
        private void LoadGame()
        {
            MainTimer.Stop();
            if (!File.Exists("save.dat")) return;
            using (var fileStream = new FileStream("save.dat", FileMode.Open, FileAccess.Read))
            {
                BinaryFormatter formatter = new BinaryFormatter();
                game = (Game)formatter.Deserialize(fileStream);
            }
            flpContracts.Controls.Clear();
            flpFlights.Controls.Clear();
            flpMarket.Controls.Clear();
            flpMyAirplanes.Controls.Clear();
            flpHistory.Controls.Clear();
            foreach (var i in game.Contracts)
            {
                flpContracts.Controls.Add(new ContractPanel(i, false));
            }
            foreach (var i in game.Flights)
            {
                flpFlights.Controls.Add(new FlightPanel(i));
            }
            foreach (var i in game.MarketAirplanes)
            {
                flpMarket.Controls.Add(new AirplanePanel(i, false));
            }
            foreach (var i in game.PlayerAirplanes)
            {
                flpMyAirplanes.Controls.Add(new AirplanePanel(i, true));
            }
            foreach (var i in game.ContractHistory)
            {
                flpHistory.Controls.Add(new ContractPanel(i, true));
            }
            UpdateInfo();
            MainTimer.Start();
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveGame();
        }

        private void loadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LoadGame();
        }
    }
}
