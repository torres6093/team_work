﻿namespace team_work
{
    partial class MainForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tpAeroCompany = new System.Windows.Forms.TabPage();
            this.scAirplanes = new System.Windows.Forms.SplitContainer();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.flpMyAirplanes = new System.Windows.Forms.FlowLayoutPanel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.flpMarket = new System.Windows.Forms.FlowLayoutPanel();
            this.tpFlights = new System.Windows.Forms.TabPage();
            this.flpFlights = new System.Windows.Forms.FlowLayoutPanel();
            this.tpContracts = new System.Windows.Forms.TabPage();
            this.flpContracts = new System.Windows.Forms.FlowLayoutPanel();
            this.tpHistory = new System.Windows.Forms.TabPage();
            this.flpHistory = new System.Windows.Forms.FlowLayoutPanel();
            this.splitContainer = new System.Windows.Forms.SplitContainer();
            this.lblFuelCost = new System.Windows.Forms.Label();
            this.lblSpeed = new System.Windows.Forms.Label();
            this.lblMoney = new System.Windows.Forms.Label();
            this.lblDate = new System.Windows.Forms.Label();
            this.MainTimer = new System.Windows.Forms.Timer(this.components);
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tabControl1.SuspendLayout();
            this.tpAeroCompany.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.scAirplanes)).BeginInit();
            this.scAirplanes.Panel1.SuspendLayout();
            this.scAirplanes.Panel2.SuspendLayout();
            this.scAirplanes.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.tpFlights.SuspendLayout();
            this.tpContracts.SuspendLayout();
            this.tpHistory.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer)).BeginInit();
            this.splitContainer.Panel1.SuspendLayout();
            this.splitContainer.Panel2.SuspendLayout();
            this.splitContainer.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tpAeroCompany);
            this.tabControl1.Controls.Add(this.tpFlights);
            this.tabControl1.Controls.Add(this.tpContracts);
            this.tabControl1.Controls.Add(this.tpHistory);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(2);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(967, 469);
            this.tabControl1.TabIndex = 0;
            // 
            // tpAeroCompany
            // 
            this.tpAeroCompany.Controls.Add(this.scAirplanes);
            this.tpAeroCompany.Location = new System.Drawing.Point(4, 25);
            this.tpAeroCompany.Margin = new System.Windows.Forms.Padding(2);
            this.tpAeroCompany.Name = "tpAeroCompany";
            this.tpAeroCompany.Padding = new System.Windows.Forms.Padding(2);
            this.tpAeroCompany.Size = new System.Drawing.Size(959, 440);
            this.tpAeroCompany.TabIndex = 0;
            this.tpAeroCompany.Text = "Авиакомпания";
            this.tpAeroCompany.UseVisualStyleBackColor = true;
            // 
            // scAirplanes
            // 
            this.scAirplanes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.scAirplanes.IsSplitterFixed = true;
            this.scAirplanes.Location = new System.Drawing.Point(2, 2);
            this.scAirplanes.Name = "scAirplanes";
            // 
            // scAirplanes.Panel1
            // 
            this.scAirplanes.Panel1.BackColor = System.Drawing.Color.Transparent;
            this.scAirplanes.Panel1.Controls.Add(this.groupBox1);
            // 
            // scAirplanes.Panel2
            // 
            this.scAirplanes.Panel2.BackColor = System.Drawing.Color.Transparent;
            this.scAirplanes.Panel2.Controls.Add(this.groupBox2);
            this.scAirplanes.Size = new System.Drawing.Size(955, 436);
            this.scAirplanes.SplitterDistance = 476;
            this.scAirplanes.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.flpMyAirplanes);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(476, 436);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Мои самолёты";
            // 
            // flpMyAirplanes
            // 
            this.flpMyAirplanes.AutoScroll = true;
            this.flpMyAirplanes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flpMyAirplanes.Location = new System.Drawing.Point(3, 18);
            this.flpMyAirplanes.Name = "flpMyAirplanes";
            this.flpMyAirplanes.Size = new System.Drawing.Size(470, 415);
            this.flpMyAirplanes.TabIndex = 1;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.flpMarket);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Location = new System.Drawing.Point(0, 0);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(475, 436);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Рынок самолётов";
            // 
            // flpMarket
            // 
            this.flpMarket.AutoScroll = true;
            this.flpMarket.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flpMarket.Location = new System.Drawing.Point(3, 18);
            this.flpMarket.Name = "flpMarket";
            this.flpMarket.Size = new System.Drawing.Size(469, 415);
            this.flpMarket.TabIndex = 2;
            // 
            // tpFlights
            // 
            this.tpFlights.Controls.Add(this.flpFlights);
            this.tpFlights.Location = new System.Drawing.Point(4, 25);
            this.tpFlights.Margin = new System.Windows.Forms.Padding(2);
            this.tpFlights.Name = "tpFlights";
            this.tpFlights.Padding = new System.Windows.Forms.Padding(2);
            this.tpFlights.Size = new System.Drawing.Size(918, 440);
            this.tpFlights.TabIndex = 1;
            this.tpFlights.Text = "Объявления";
            this.tpFlights.UseVisualStyleBackColor = true;
            // 
            // flpFlights
            // 
            this.flpFlights.AutoScroll = true;
            this.flpFlights.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flpFlights.Location = new System.Drawing.Point(2, 2);
            this.flpFlights.Margin = new System.Windows.Forms.Padding(2);
            this.flpFlights.Name = "flpFlights";
            this.flpFlights.Size = new System.Drawing.Size(914, 436);
            this.flpFlights.TabIndex = 0;
            // 
            // tpContracts
            // 
            this.tpContracts.Controls.Add(this.flpContracts);
            this.tpContracts.Location = new System.Drawing.Point(4, 25);
            this.tpContracts.Name = "tpContracts";
            this.tpContracts.Padding = new System.Windows.Forms.Padding(3);
            this.tpContracts.Size = new System.Drawing.Size(918, 440);
            this.tpContracts.TabIndex = 2;
            this.tpContracts.Text = "Мои полёты";
            this.tpContracts.UseVisualStyleBackColor = true;
            // 
            // flpContracts
            // 
            this.flpContracts.AutoScroll = true;
            this.flpContracts.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flpContracts.Location = new System.Drawing.Point(3, 3);
            this.flpContracts.Margin = new System.Windows.Forms.Padding(2);
            this.flpContracts.Name = "flpContracts";
            this.flpContracts.Size = new System.Drawing.Size(912, 434);
            this.flpContracts.TabIndex = 1;
            // 
            // tpHistory
            // 
            this.tpHistory.Controls.Add(this.flpHistory);
            this.tpHistory.Location = new System.Drawing.Point(4, 25);
            this.tpHistory.Name = "tpHistory";
            this.tpHistory.Padding = new System.Windows.Forms.Padding(3);
            this.tpHistory.Size = new System.Drawing.Size(918, 440);
            this.tpHistory.TabIndex = 3;
            this.tpHistory.Text = "История";
            this.tpHistory.UseVisualStyleBackColor = true;
            // 
            // flpHistory
            // 
            this.flpHistory.AutoScroll = true;
            this.flpHistory.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flpHistory.Location = new System.Drawing.Point(3, 3);
            this.flpHistory.Name = "flpHistory";
            this.flpHistory.Size = new System.Drawing.Size(912, 434);
            this.flpHistory.TabIndex = 0;
            // 
            // splitContainer
            // 
            this.splitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer.IsSplitterFixed = true;
            this.splitContainer.Location = new System.Drawing.Point(0, 28);
            this.splitContainer.Margin = new System.Windows.Forms.Padding(2);
            this.splitContainer.Name = "splitContainer";
            this.splitContainer.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer.Panel1
            // 
            this.splitContainer.Panel1.Controls.Add(this.lblFuelCost);
            this.splitContainer.Panel1.Controls.Add(this.lblSpeed);
            this.splitContainer.Panel1.Controls.Add(this.lblMoney);
            this.splitContainer.Panel1.Controls.Add(this.lblDate);
            // 
            // splitContainer.Panel2
            // 
            this.splitContainer.Panel2.Controls.Add(this.tabControl1);
            this.splitContainer.Size = new System.Drawing.Size(967, 506);
            this.splitContainer.SplitterDistance = 34;
            this.splitContainer.SplitterWidth = 3;
            this.splitContainer.TabIndex = 1;
            // 
            // lblFuelCost
            // 
            this.lblFuelCost.AutoSize = true;
            this.lblFuelCost.Location = new System.Drawing.Point(730, 7);
            this.lblFuelCost.Name = "lblFuelCost";
            this.lblFuelCost.Size = new System.Drawing.Size(121, 17);
            this.lblFuelCost.TabIndex = 3;
            this.lblFuelCost.Text = "Цена на топливо";
            // 
            // lblSpeed
            // 
            this.lblSpeed.AutoSize = true;
            this.lblSpeed.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.lblSpeed.Location = new System.Drawing.Point(129, 7);
            this.lblSpeed.Name = "lblSpeed";
            this.lblSpeed.Size = new System.Drawing.Size(94, 17);
            this.lblSpeed.TabIndex = 2;
            this.lblSpeed.Text = "Скорость: 1Х";
            this.lblSpeed.Click += new System.EventHandler(this.lblSpeed_Click);
            // 
            // lblMoney
            // 
            this.lblMoney.AutoSize = true;
            this.lblMoney.Location = new System.Drawing.Point(285, 7);
            this.lblMoney.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblMoney.Name = "lblMoney";
            this.lblMoney.Size = new System.Drawing.Size(55, 17);
            this.lblMoney.TabIndex = 1;
            this.lblMoney.Text = "Деньги";
            // 
            // lblDate
            // 
            this.lblDate.AutoSize = true;
            this.lblDate.Location = new System.Drawing.Point(484, 7);
            this.lblDate.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblDate.Name = "lblDate";
            this.lblDate.Size = new System.Drawing.Size(42, 17);
            this.lblDate.TabIndex = 0;
            this.lblDate.Text = "Дата";
            // 
            // MainTimer
            // 
            this.MainTimer.Interval = 4000;
            this.MainTimer.Tick += new System.EventHandler(this.MainTimer_Tick);
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(967, 28);
            this.menuStrip1.TabIndex = 2;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.saveToolStripMenuItem,
            this.loadToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(57, 24);
            this.fileToolStripMenuItem.Text = "Файл";
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(158, 26);
            this.saveToolStripMenuItem.Text = "Сохранить";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
            // 
            // loadToolStripMenuItem
            // 
            this.loadToolStripMenuItem.Name = "loadToolStripMenuItem";
            this.loadToolStripMenuItem.Size = new System.Drawing.Size(158, 26);
            this.loadToolStripMenuItem.Text = "Загрузить";
            this.loadToolStripMenuItem.Click += new System.EventHandler(this.loadToolStripMenuItem_Click);
            // 
            // MainForm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(967, 534);
            this.Controls.Add(this.splitContainer);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "MainForm";
            this.Text = "MainForm";
            this.tabControl1.ResumeLayout(false);
            this.tpAeroCompany.ResumeLayout(false);
            this.scAirplanes.Panel1.ResumeLayout(false);
            this.scAirplanes.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.scAirplanes)).EndInit();
            this.scAirplanes.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.tpFlights.ResumeLayout(false);
            this.tpContracts.ResumeLayout(false);
            this.tpHistory.ResumeLayout(false);
            this.splitContainer.Panel1.ResumeLayout(false);
            this.splitContainer.Panel1.PerformLayout();
            this.splitContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer)).EndInit();
            this.splitContainer.ResumeLayout(false);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tpAeroCompany;
        private System.Windows.Forms.TabPage tpFlights;
        private System.Windows.Forms.SplitContainer splitContainer;
        private System.Windows.Forms.Label lblMoney;
        private System.Windows.Forms.Label lblDate;
        private System.Windows.Forms.FlowLayoutPanel flpFlights;
        private System.Windows.Forms.SplitContainer scAirplanes;
        private System.Windows.Forms.FlowLayoutPanel flpMyAirplanes;
        private System.Windows.Forms.FlowLayoutPanel flpMarket;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TabPage tpContracts;
        private System.Windows.Forms.FlowLayoutPanel flpContracts;
        private System.Windows.Forms.Timer MainTimer;
        private System.Windows.Forms.Label lblSpeed;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadToolStripMenuItem;
        private System.Windows.Forms.Label lblFuelCost;
        private System.Windows.Forms.TabPage tpHistory;
        private System.Windows.Forms.FlowLayoutPanel flpHistory;
    }
}

