﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using team_work.Classes;
using team_work.Interface;

namespace team_work
{
    public partial class ContractForm : Form
    {
        private List<Airplane> AvalibleAirplanes = new List<Airplane>();
        public Airplane Chosen = null;
        public int StartHour;
        public ContractForm()
        {
            InitializeComponent();

        }
        public void Init(List<Airplane> airplanes, int hour)
        {
            numericUpDown1.Minimum = hour;
            if (airplanes.Count == 0)
            {
                lblNoitems.Visible = true;
                flpAirplanes.Visible = false;
            }
            else
            {
                AvalibleAirplanes = airplanes;
                foreach (var i in AvalibleAirplanes)
                {
                    flpAirplanes.Controls.Add(new AirplaneSetPanel(i, this));
                }
            }
        }
        public void ChangeChoice(AirplaneSetPanel panel)
        {
            foreach (var p in flpAirplanes.Controls)
            {
                (p as AirplaneSetPanel).Set = false;
            }
            panel.Set = true;
            Chosen = panel.airplane;
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            StartHour = (int)numericUpDown1.Value;
            if (Chosen == null)
            {
                DialogResult = DialogResult.Cancel;
                return;
            }
            if (AvalibleAirplanes.Count > 0)
            {
                DialogResult = DialogResult.OK;
            }
            else
            {
                DialogResult = DialogResult.Cancel;
            }
        }
    }
}
