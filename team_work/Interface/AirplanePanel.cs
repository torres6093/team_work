﻿using System;
using System.Windows.Forms;
using team_work.Classes;

namespace team_work.Interface
{
    public class AirplanePanel : Panel
    {
        private Label lblName;
        private Label lblCity;
        private Label lblPrice;
        private Label lblMaxSpeed;
        private Label lblPurchaseType;
        private Label lblPassengers;
        private Label lblCargo;
        private Label lblFuelcost;
        private Button btnSell;
        private Button btnMove;
        private Airplane airplane;
        private bool my; // Этот самолёт нам принадлежит?

        #region Настройка панели
        private void Setup()
        {
            lblName = new Label();
            lblMaxSpeed = new Label();
            lblPrice = new Label();
            lblCity = new Label();
            lblPurchaseType = new Label();
            lblPassengers = new Label();
            lblCargo = new Label();
            lblFuelcost = new Label();
            btnSell = new Button();
            btnMove = new Button();

            Location = new System.Drawing.Point(3, 3);
            Size = new System.Drawing.Size(430, 140);
            TabIndex = 0;
            BackColor = System.Drawing.Color.PaleTurquoise;
            // 
            // lblName
            // 
            lblName.AutoSize = true;
            lblName.BackColor = System.Drawing.Color.Transparent;
            lblName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, 204);
            lblName.Location = new System.Drawing.Point(80, 19);
            lblName.Name = "lblName";
            lblName.Size = new System.Drawing.Size(66, 24);
            lblName.TabIndex = 0;
            // 
            // lblMaxSpeed
            // 
            lblMaxSpeed.AutoSize = true;
            lblMaxSpeed.BackColor = System.Drawing.Color.Transparent;
            lblMaxSpeed.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 204);
            lblMaxSpeed.Location = new System.Drawing.Point(30, 50);
            lblMaxSpeed.Name = "lblFDate";
            lblMaxSpeed.Size = new System.Drawing.Size(67, 24);
            lblMaxSpeed.TabIndex = 1;
            // 
            // lblPrice
            // 
            lblPrice.AutoSize = true;
            lblPrice.BackColor = System.Drawing.Color.Transparent;
            lblPrice.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 204);
            lblPrice.Location = new System.Drawing.Point(150, 80);
            lblPrice.Name = "lblPrice";
            lblPrice.Size = new System.Drawing.Size(53, 20);
            lblPrice.TabIndex = 2;
            // 
            // lblCity
            // 
            lblCity.AutoSize = true;
            lblCity.BackColor = System.Drawing.Color.Transparent;
            lblCity.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 204);
            lblCity.Location = new System.Drawing.Point(30, 80);
            lblCity.Name = "lblCity";
            lblCity.Size = new System.Drawing.Size(53, 20);
            lblCity.TabIndex = 3;
            // 
            // lblPurchaseType
            // 
            lblPurchaseType.AutoSize = true;
            lblPurchaseType.BackColor = System.Drawing.Color.Transparent;
            lblPurchaseType.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 204);
            lblPurchaseType.Location = new System.Drawing.Point(150, 50);
            lblPurchaseType.Name = "lblPurchaseType";
            lblPurchaseType.Size = new System.Drawing.Size(53, 20);
            lblPurchaseType.TabIndex = 3;
            // 
            // lblPassengers
            // 
            lblPassengers.AutoSize = true;
            lblPassengers.BackColor = System.Drawing.Color.Transparent;
            lblPassengers.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 204);
            lblPassengers.Location = new System.Drawing.Point(270, 50);
            lblPassengers.Name = "lblPassengers";
            lblPassengers.Size = new System.Drawing.Size(53, 20);
            lblPassengers.TabIndex = 3;
            // 
            // lblCargo
            // 
            lblCargo.AutoSize = true;
            lblCargo.BackColor = System.Drawing.Color.Transparent;
            lblCargo.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 204);
            lblCargo.Location = new System.Drawing.Point(270, 80);
            lblCargo.Name = "lblCargo";
            lblCargo.Size = new System.Drawing.Size(53, 20);
            lblCargo.TabIndex = 3;
            // 
            // lblFuelcost
            // 
            lblFuelcost.AutoSize = true;
            lblFuelcost.BackColor = System.Drawing.Color.Transparent;
            lblFuelcost.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 204);
            lblFuelcost.Location = new System.Drawing.Point(270, 110);
            lblFuelcost.Name = "lblFuelcost";
            lblFuelcost.Size = new System.Drawing.Size(53, 20);
            lblFuelcost.TabIndex = 3;
            // 
            // btnTake
            // 
            btnSell.Location = new System.Drawing.Point(150, 110);
            btnSell.Name = "btnTake";
            btnSell.Size = new System.Drawing.Size(80, 25);
            btnSell.TabIndex = 4;
            btnSell.Text = "Взять";
            btnSell.UseVisualStyleBackColor = true;
            btnSell.Click += btnTake_Click;
            // 
            // btnMove
            // 
            btnMove.Location = new System.Drawing.Point(30, 110);
            btnMove.Name = "btnTake";
            btnMove.Size = new System.Drawing.Size(80, 25);
            btnMove.TabIndex = 4;
            btnMove.Text = "Перевезти";
            btnMove.UseVisualStyleBackColor = true;
            btnMove.Click += btnMove_Click;
        }
        #endregion

        private void btnMove_Click(object sender, EventArgs e)
        {
            MoveForm form = new MoveForm();
            form.Init(airplane.City);
            if (form.ShowDialog() == DialogResult.OK)
            {
                airplane.City = form.CityChosen;
            }
            UpdateInfo();
        }

        private void btnTake_Click(object sender, EventArgs e)
        {
            var form = (Application.OpenForms[0] as MainForm);
            if (!my)
            {
                if (form.game.GetAirplane(airplane))
                {
                    form.ToMy(this);
                    my = !my;
                }
            }
            else
            {
                if (form.game.ReturnAirplane(airplane))
                {
                    form.ToMarket(this);
                    my = !my;
                }
            }
            UpdateInfo();
        }
        public AirplanePanel(Airplane airplane, bool my)
        {
            this.airplane = airplane;
            this.my = my;
            Setup();
            UpdateInfo();
            lblPassengers.Text = $"Пассажиров {airplane.CapacityPassenger} (чел.)";
            lblCargo.Text = $"Груз {airplane.CapacityCargo} (т)";
            lblFuelcost.Text = $"Расход {airplane.FuelCost} (л/км)";
            Controls.Add(btnSell);
            Controls.Add(btnMove);
            Controls.Add(lblCity);
            Controls.Add(lblPrice);
            Controls.Add(lblMaxSpeed);
            Controls.Add(lblName);
            Controls.Add(lblPurchaseType);
            Controls.Add(lblPassengers);
            Controls.Add(lblCargo);
            Controls.Add(lblFuelcost);
        }
        public void UpdateInfo()
        {
            lblName.Text = airplane.Name;
            lblMaxSpeed.Text = "Дальность: " + airplane.MaxSpeed.ToString();
            lblPrice.Text = "Цена: $" + airplane.PurchaseType.Price.ToString();
            lblCity.Text = airplane.City;
            if (my)
            {
                if (airplane.PurchaseType is Buy)
                    btnSell.Text = "Продать";
                else
                {
                    if(airplane.PurchaseType is Leasing)
                    {
                        if ((airplane.PurchaseType as Leasing).CanTake)
                        {
                            btnSell.Text = "Забрать";
                        }
                        else 
                            btnSell.Text = "Вернуть";
                    }
                    else
                        btnSell.Text = "Вернуть";
                }
                btnMove.Visible = true;
            }
            else
            {
                if (airplane.PurchaseType is Buy)
                    btnSell.Text = "Купить";
                else
                {
                    btnSell.Text = "Взять";
                }
                btnMove.Visible = false;
            }
            if (airplane.PurchaseType is Buy)
            {
                lblPurchaseType.Text = "Покупка";
            }
            if (airplane.PurchaseType is Rent)
            {
                lblPurchaseType.Text = "Аренда (Мес.)";
            }
            if (airplane.PurchaseType is Leasing)
            {
                lblPurchaseType.Text = "Лизинг (Мес.)";
            }
        }
    }
}
