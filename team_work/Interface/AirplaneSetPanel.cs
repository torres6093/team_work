﻿using System;
using System.Windows.Forms;
using team_work.Classes;

namespace team_work.Interface
{
    public class AirplaneSetPanel : Panel
    {
        private Label lblName;
        private Label lblCity;
        private Label lblMaxSpeed;
        private RadioButton rbSet;
        public Airplane airplane;
        public bool Set { get => rbSet.Checked; set => rbSet.Checked = value; }
        private ContractForm form;

        #region Настройка панели
        private void Setup()
        {
            lblName = new Label();
            lblMaxSpeed = new Label();
            lblCity = new Label();
            rbSet = new RadioButton();

            Location = new System.Drawing.Point(3, 3);
            Size = new System.Drawing.Size(260, 140);
            TabIndex = 0;
            BackColor = System.Drawing.Color.PaleTurquoise;
            // 
            // lblName
            // 
            lblName.AutoSize = true;
            lblName.BackColor = System.Drawing.Color.Transparent;
            lblName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, 204);
            lblName.Location = new System.Drawing.Point(80, 19);
            lblName.Name = "lblName";
            lblName.Size = new System.Drawing.Size(66, 24);
            lblName.TabIndex = 0;
            // 
            // lblMaxSpeed
            // 
            lblMaxSpeed.AutoSize = true;
            lblMaxSpeed.BackColor = System.Drawing.Color.Transparent;
            lblMaxSpeed.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 204);
            lblMaxSpeed.Location = new System.Drawing.Point(30, 50);
            lblMaxSpeed.Name = "lblFDate";
            lblMaxSpeed.Size = new System.Drawing.Size(67, 24);
            lblMaxSpeed.TabIndex = 1;
            // 
            // lblCapacity
            // 
            lblCity.AutoSize = true;
            lblCity.BackColor = System.Drawing.Color.Transparent;
            lblCity.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 204);
            lblCity.Location = new System.Drawing.Point(30, 80);
            lblCity.Name = "lblCapacity";
            lblCity.Size = new System.Drawing.Size(53, 20);
            lblCity.TabIndex = 3;
            // 
            // btnTake
            // 
            rbSet.Location = new System.Drawing.Point(150, 80);
            rbSet.Name = "btnTake";
            rbSet.Size = new System.Drawing.Size(80, 25);
            rbSet.TabIndex = 4;
            rbSet.Text = "Назначить";
            rbSet.UseVisualStyleBackColor = true;
            rbSet.Click += RbSet_Click;
        }
        #endregion

        private void RbSet_Click(object sender, EventArgs e)
        {
            if ((sender as RadioButton).Checked)
            {
                form.ChangeChoice(this);
            }
        }

        public AirplaneSetPanel(Airplane airplane, ContractForm form)
        {
            this.airplane = airplane;
            this.form = form;
            Setup();
            lblName.Text = airplane.Name;
            lblMaxSpeed.Text = "Дальность: " + airplane.MaxSpeed.ToString();
            lblCity.Text = airplane.City;
            Controls.Add(rbSet);
            Controls.Add(lblCity);
            Controls.Add(lblMaxSpeed);
            Controls.Add(lblName);
        }
    }
}
