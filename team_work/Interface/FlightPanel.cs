﻿using System;
using System.Windows.Forms;
using team_work.Classes;

namespace team_work.Interface
{
    public class FlightPanel : Panel
    {
        private Label lblName;
        private Label lblCapacity;
        private Label lblPrice;
        private Label lblFDate;
        private Label lblRegular;
        private Label lblDistance;
        private Button btnTake;
        private Flight flight;

        #region Настройка панели
        private void Setup()
        {
            lblName = new Label();
            lblFDate = new Label();
            lblPrice = new Label();
            lblCapacity = new Label();
            lblRegular = new Label();
            lblDistance = new Label();
            btnTake = new Button();

            Location = new System.Drawing.Point(3, 3);
            Size = new System.Drawing.Size(260, 140);
            TabIndex = 0;
            BackColor = System.Drawing.Color.PaleGreen;
            // 
            // lblName
            // 
            lblName.AutoSize = true;
            lblName.BackColor = System.Drawing.Color.Transparent;
            lblName.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, 204);
            lblName.Location = new System.Drawing.Point(30, 19);
            lblName.Name = "lblName";
            lblName.Size = new System.Drawing.Size(66, 24);
            lblName.TabIndex = 0;
            // 
            // lblDistance
            // 
            lblDistance.AutoSize = true;
            lblDistance.BackColor = System.Drawing.Color.Transparent;
            lblDistance.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 204);
            lblDistance.Location = new System.Drawing.Point(160, 19);
            lblDistance.Name = "lblDistance";
            lblDistance.Size = new System.Drawing.Size(66, 24);
            lblDistance.TabIndex = 0;
            // 
            // lblFDate
            // 
            lblFDate.AutoSize = true;
            lblFDate.BackColor = System.Drawing.Color.Transparent;
            lblFDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 204);
            lblFDate.Location = new System.Drawing.Point(30, 50);
            lblFDate.Name = "lblFDate";
            lblFDate.Size = new System.Drawing.Size(67, 24);
            lblFDate.TabIndex = 1;
            // 
            // lblPrice
            // 
            lblPrice.AutoSize = true;
            lblPrice.BackColor = System.Drawing.Color.Transparent;
            lblPrice.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 204);
            lblPrice.Location = new System.Drawing.Point(150, 50);
            lblPrice.Name = "lblPrice";
            lblPrice.Size = new System.Drawing.Size(53, 20);
            lblPrice.TabIndex = 2;
            // 
            // lblCapacity
            // 
            lblCapacity.AutoSize = true;
            lblCapacity.BackColor = System.Drawing.Color.Transparent;
            lblCapacity.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 204);
            lblCapacity.Location = new System.Drawing.Point(30, 80);
            lblCapacity.Name = "lblCapacity";
            lblCapacity.Size = new System.Drawing.Size(53, 20);
            lblCapacity.TabIndex = 3;
            // 
            // lblRegular
            // 
            lblRegular.AutoSize = true;
            lblRegular.BackColor = System.Drawing.Color.Transparent;
            lblRegular.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 204);
            lblRegular.Location = new System.Drawing.Point(30, 100);
            lblRegular.Name = "lblRegular";
            lblRegular.Size = new System.Drawing.Size(53, 20);
            lblRegular.TabIndex = 3;
            // 
            // btnTake
            // 
            btnTake.Location = new System.Drawing.Point(150, 80);
            btnTake.Name = "btnTake";
            btnTake.Size = new System.Drawing.Size(80, 25);
            btnTake.TabIndex = 4;
            btnTake.Text = "Взять";
            btnTake.UseVisualStyleBackColor = true;
            btnTake.Click += btnTake_Click;
        }
        #endregion

        private void btnTake_Click(object sender, EventArgs e)
        {
            (Application.OpenForms[0] as MainForm).game.AddContract(flight);
            (Application.OpenForms[0] as MainForm).AddContract(this, flight);
        }
        private string DaysLabel(int d)
        {
            if (d == 1) return " день";
            if (d < 5) return " дня";
            return " дней";
        }
        public FlightPanel(Flight flight)
        {
            this.flight = flight;
            Setup();
            lblName.Text = flight.StartCity + " - " + flight.EndCity;
            lblFDate.Text = flight.StartDate.ToLongDateString();
            lblPrice.Text = "Доход: $" + flight.Price.ToString();
            lblRegular.Visible = false;
            if (flight.Type is AirlanerType)
            {
                lblCapacity.Text = "Пассажиры " + flight.Type.CapacityRequired;
                var f = (flight.Type as AirlanerType);
                if (f.Regular)
                {
                    lblRegular.Text = "Регуляный\nраз в " + f.Regularity + DaysLabel(f.Regularity);
                    lblRegular.Visible = true;
                }
            }
            else
            {
                lblCapacity.Text = $"Груз {flight.Type.CapacityRequired} (т)";
            }
            lblDistance.Text = $"({flight.Distance} км)";
            Controls.Add(btnTake);
            Controls.Add(lblCapacity);
            Controls.Add(lblRegular);
            Controls.Add(lblPrice);
            Controls.Add(lblFDate);
            Controls.Add(lblName);
            Controls.Add(lblDistance);
        }
    }
}
