﻿using System;

namespace team_work.Classes
{
    public interface IFlightType
    {
        int CapacityRequired { get; set; }
    }

    [Serializable]
    public class CargoType : IFlightType
    {
        public int CapacityRequired { get; set; }
        public CargoType(int capacity)
        {
            CapacityRequired = capacity;
        }
        public CargoType()
        {
            CapacityRequired = 0;
        }
    }

    [Serializable]
    public class AirlanerType : IFlightType
    {
        public int CapacityRequired { get; set; }
        public bool Regular = false;
        public int Regularity = 0;  // В днях
        public DateTime NextDate;
        public AirlanerType(int capacity)
        {
            CapacityRequired = capacity;
        }
        public AirlanerType()
        {
            CapacityRequired = 0;
        }
    }

    [Serializable]
    public enum FlightStatus
    {
        Wait,  // Ожидаем вылета
        Progress, // В полёте
        WaitReturn, 
        Return, // Возврат при регулярном рейсе
        Complete, // Выполнен
        None, // Ждем назначения
        Expired // Провален
    }
    [Serializable]
    public class Flight
    {
        public DateTime StartDate; // Дата и время вылета
        public DateTime EndDate; // Дата и время прилёта
        public IFlightType Type; // Тип (грузовой / пассажирский)
        public int Distance; // Сколько нужно пролететь
        public DateTime ExpireDate; // Время исчезновениия / провала контракта
        public int ExpirePrice; // Сколько нужно отдать за провал
        public string StartCity; // Город вылета
        public string EndCity; // Город прилета
        public int Price; // Сколько заплатят за выполнение
        public FlightStatus Status;
        public Airplane AssignedAirplane;
        public void StartContract(int StartHour, Airplane airplane, DateTime Now)
        {
            StartDate = StartDate.AddHours(StartHour);
            double travelTime = Math.Ceiling((double)Distance / airplane.MaxSpeed);
            EndDate = StartDate.AddHours(travelTime);
            AssignedAirplane = airplane;
            AssignedAirplane.Assigned = true;
            
            if(Now.Hour == StartDate.Hour)
            {
                Status = FlightStatus.Progress;
                AssignedAirplane.Status = PlaneStatus.InFlight;
            }
            else
            {
                Status = FlightStatus.Wait;
                AssignedAirplane.Status = PlaneStatus.Wait;
            }
        }
        public void Return(DateTime StartDate)
        {
            this.StartDate = StartDate;
            double travelTime = Math.Ceiling((double)Distance / AssignedAirplane.MaxSpeed);
            EndDate = StartDate.AddHours(travelTime);
        }
    }
}
