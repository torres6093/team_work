﻿using System;

namespace team_work.Classes
{
    [Serializable]
    public enum PlaneStatus
    {
        InFlight,
        Wait,
        City
    }

    public interface IPurchaseType
    {
        int Price { get; set; }
    }
    [Serializable]
    public class Buy : IPurchaseType
    {
        public int Price { get; set; }
    }
    [Serializable]
    public class Rent : IPurchaseType
    {
        public DateTime StartDate;
        public int Price { get; set; }
        public void NewMonth()
        {
            StartDate = StartDate.AddMonths(1);
        }
    }
    [Serializable]
    public class Leasing : IPurchaseType
    {
        public int TotalPrice;
        public int RemainPrice;
        public bool CanTake { get; private set; }
        public DateTime StartDate;
        private int FixPrice;
        private int Percent;
        public Leasing()
        {
            CanTake = false;
        }
        public void NewMonth()
        {
            RemainPrice -= FixPrice;
            if (RemainPrice <= 0)
            {
                CanTake = true;
            }
            StartDate = StartDate.AddMonths(1);
        }
        public int Price
        {
            get
            {
                return FixPrice;
                /*  Проценты, м.б. неверно, поправлю, если найду хорошую формулу
                if (FixPrice > RemainPrice) return FixPrice;
                int v = FixPrice + RemainPrice * Percent / 100;
                return v;
                */
            }
            set { FixPrice = value; }
        }
    }

    // Описане самолёта
    [Serializable]
    public class Airplane
    {
        public int id; // id для поиска
        public int MaxFlightDistance; //Максимально возможная дальность полёта
        public int FuelCost; // Расход топлива за км
        public int MaxSpeed; //Максимально возможная скорость
        public int ServiceCost; // Расходы на предполётную подготовку
        public PlaneStatus Status; //В полёте/в городе
        public string City; // Город в котором мы находимся
        public int CapacityPassenger; // Вместимость в пассажирах
        public int CapacityCargo; // Вместимость в тоннах
        public string Name; // Название
        public string BaseCity; // Город приписки
        public IPurchaseType PurchaseType;// Тип покупки
        public bool Assigned; // Самолёт уже назначен
    }
}
