﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace team_work.Classes
{
    [Serializable]
    public class Game
    {
        public List<Airplane> PlayerAirplanes { get; private set; }
        public List<Airplane> MarketAirplanes { get; private set; }
        public DateTime Date { get; private set; }
        public int Money { get; private set; }
        public List<Flight> Contracts { get; private set; } // Взятые нами
        public List<Flight> Flights { get; private set; } // Все доступные полёты
        public List<Flight> ContractHistory { get; private set; }
        private bool Pause;
        public List<string> CityNamesList = new List<string>();
        public Dictionary<string, Dictionary<string, int>> CityDistances;
        public List<string> PlaneNamesList = new List<string>();
        public int FuelPrice { get; private set; } // Текущая цена на топливо за км
        public bool Debt { get; private set; }
        public DateTime DebtStart { get; private set; }


        private void GenerateDistances(List<string> cities, Random random)
        {
            for (int i = 0; i < cities.Count; i++)
            {
                CityDistances.Add(cities[i], new Dictionary<string, int>());
            }
            for (int i = 0; i < cities.Count; i++)
            {
                for (int j = i + 1; j < cities.Count; j++)
                {
                    if (CityDistances.ContainsKey(cities[i]))
                    {
                        int r = random.Next(500, 1500);
                        CityDistances[cities[i]].Add(cities[j], r);
                        CityDistances[cities[j]].Add(cities[i], r);
                    }
                }
            }
        }

        private Airplane GenerateAirplane(Random random, int num)
        {
            IPurchaseType pt;
            switch (random.Next(3))
            {
                case (0):
                {
                    pt = new Buy { Price = random.Next(7000, 18000) };
                    break;
                }
                case 1:
                {
                    pt = new Rent { Price = random.Next(700, 1800), StartDate = Date };
                    break;
                }
                case 2:
                {
                    int pr = random.Next(700, 1800);
                    int tp = random.Next(7000, 18000);
                    pt = new Leasing { Price = pr, TotalPrice = tp, StartDate = Date, RemainPrice = tp };
                    break;
                }
                default:
                    pt = new Buy { Price = 9999 };
                    break;
            }

            Airplane airplane = new Airplane
            {
                City = CityNamesList[random.Next(CityNamesList.Count)],
                PurchaseType = pt,
                Name = "самолёт" + num,
                MaxSpeed = random.Next(400, 900),
                CapacityCargo = random.Next(300, 1000),
                CapacityPassenger = random.Next(100, 600),
                FuelCost = random.Next(1, 4),
                ServiceCost = random.Next(1, 3),
                Status = PlaneStatus.City,
                MaxFlightDistance = random.Next(500, 1500)
            };
            return airplane;
        }

        // Нужен для работы сохранений
        public Game()
        {
            Money = 1000000;
            Date = DateTime.Today.AddHours(DateTime.Now.Hour);
            PlayerAirplanes = new List<Airplane>();
            MarketAirplanes = new List<Airplane>();
            Contracts = new List<Flight>();
            Flights = new List<Flight>();
            CityDistances = new Dictionary<string, Dictionary<string, int>>();
            ContractHistory = new List<Flight>();
        }

        public Game(int money, DateTime date)
        {
            Money = money;
            Date = date;
            PlayerAirplanes = new List<Airplane>();
            MarketAirplanes = new List<Airplane>();
            Contracts = new List<Flight>();
            Flights = new List<Flight>();
            CityDistances = new Dictionary<string, Dictionary<string, int>>();
            ContractHistory = new List<Flight>();

            for (int i = 0; i < 10; i++)
            {
                CityNamesList.Add("город" + i);
            }
            Random random = new Random(DateTime.Now.Millisecond);
            FuelPrice = random.Next(3, 8);
            GenerateDistances(CityNamesList, random);
            for (int i = 0; i < 10; i++)
            {
                MarketAirplanes.Add(GenerateAirplane(random, i));
            }
        }
        public void AddAirplane(Airplane airplane)
        {
            PlayerAirplanes.Add(airplane);
        }
        public bool GetAirplane(Airplane airplane)
        {
            if (airplane.PurchaseType is Buy)
            {
                if (Money < airplane.PurchaseType.Price)
                {
                    MessageBox.Show("Недостаточно денег");
                    return false;
                }
                Money -= airplane.PurchaseType.Price;
            }
            PlayerAirplanes.Add(airplane);
            MarketAirplanes.Remove(airplane);
            return true;
        }
        public bool ReturnAirplane(Airplane airplane)
        {
            if (Contracts.Find((c) => { return c.AssignedAirplane == airplane; }) != null)
            {
                MessageBox.Show("Нельзя продать самолёт который назначен на полёт");
                return false;
            }
            if (airplane.PurchaseType is Leasing)
            {
                var r = airplane.PurchaseType as Leasing;
                if (r.StartDate.Month == Date.Month)
                {
                    Money -= r.Price;
                }
                if (r.CanTake)
                {
                    airplane.PurchaseType = new Buy
                    {
                        Price = r.TotalPrice
                    };
                    return false;
                }
            }
            if (airplane.PurchaseType is Rent)
            {
                var r = airplane.PurchaseType as Rent;
                if (r.StartDate.Month == Date.Month)
                {
                    Money -= r.Price;
                }
            }
            PlayerAirplanes.Remove(airplane);
            MarketAirplanes.Add(airplane);
            if (airplane.PurchaseType is Buy)
            {
                Money += airplane.PurchaseType.Price;
            }
            return true;
        }
        public void AddContract(Flight flight)
        {
            Flights.Remove(flight);
            Contracts.Add(flight);
            flight.Status = FlightStatus.None;
        }
        public void CancelContract(Flight flight)
        {
            Flights.Add(flight);
            Contracts.Remove(flight);
            flight.Status = FlightStatus.None;
        }
        public void ArciveContract(Flight flight)
        {
            Contracts.Remove(flight);
            ContractHistory.Add(flight);
        }

        // Найти самоёты подходящие к конракту
        public List<Airplane> GetAirplanesForContract(Flight contract)
        {
            return PlayerAirplanes.FindAll((a) =>
            {
                if (a.City == contract.StartCity && a.Status == PlaneStatus.City && !a.Assigned)
                {
                    if (contract.Type is AirlanerType && contract.Type.CapacityRequired <= a.CapacityPassenger) return true;
                    if (contract.Type is CargoType && contract.Type.CapacityRequired <= a.CapacityCargo) return true;
                    return false;
                }
                return false;
            });
        }

        private Flight GenerateFlight(Random random)
        {
            IFlightType type = null;
            switch (random.Next(3))
            {
                case 0:
                {
                    var t = new AirlanerType(random.Next(100, 600));
                    t.Regular = true;
                    t.Regularity = random.Next(3, 8);
                    t.NextDate = Date.Date;
                    type = t;
                    break;
                }
                case 1:
                {
                    type = new CargoType(random.Next(300, 1000)); ;
                    break;
                }
                case 2:
                {
                    var t = new AirlanerType(random.Next(100, 600));
                    t.Regular = false;
                    t.Regularity = 0;
                    t.NextDate = Date.Date;
                    type = t;
                    break;
                }
            }
            string s = CityNamesList[random.Next(CityNamesList.Count)];
            CityNamesList.Remove(s);
            Flight flight = new Flight
            {
                StartCity = s,
                EndCity = CityNamesList[random.Next(CityNamesList.Count)],
                StartDate = Date.Date,
                Type = type,
                Price = random.Next(10000, 70000),
                ExpireDate = Date.Date.AddDays(1),
                ExpirePrice = random.Next(10000, 30000)
            };
            CityNamesList.Add(s);
            flight.Distance = CityDistances[flight.StartCity][flight.EndCity];
            return flight;
        }
        public void RegenerateConrtracts(int n)
        {
            Flights.Clear();
            Random random = new Random();
            for (int i = 0; i < n; i++)
            {
                Flight f = GenerateFlight(random);
                Flights.Add(f);
            }
        }

        // Основная функция игрового процесса
        public void Tick()
        {
            if (Pause) return;
            Date = Date.AddHours(1);
            Random rnd = new Random(DateTime.Now.Millisecond);
            FuelPrice = rnd.Next(3, 8);
            foreach (var i in Contracts)
            {
                switch (i.Status)
                {
                    case FlightStatus.Wait:
                    {
                        // Пришло время вылета
                        if (i.StartDate <= Date)
                        {
                            i.Status = FlightStatus.Progress;
                            i.AssignedAirplane.Status = PlaneStatus.InFlight;
                            if (i.Type is AirlanerType)
                                Money -= i.AssignedAirplane.ServiceCost * i.AssignedAirplane.CapacityPassenger;
                            if (i.Type is CargoType)
                                Money -= i.AssignedAirplane.ServiceCost * i.AssignedAirplane.CapacityCargo;
                            Money -= FuelPrice * i.Distance * i.AssignedAirplane.FuelCost;
                        }
                        break;
                    }
                    case FlightStatus.Progress:
                    {
                        if (i.EndDate <= Date)
                        {
                            if (i.Type is AirlanerType)
                                Money += i.Type.CapacityRequired * rnd.Next(1, 4); // Цена билета
                            if ((i.Type is AirlanerType) && (i.Type as AirlanerType).Regular)
                            {
                                i.Status = FlightStatus.WaitReturn;
                                Money += i.Price;
                            }
                            else
                            {
                                i.Status = FlightStatus.Complete;
                                Money += i.Price;
                                i.AssignedAirplane.City = i.EndCity;
                                i.AssignedAirplane.Status = PlaneStatus.City;
                                i.AssignedAirplane.Assigned = false;
                                i.AssignedAirplane = null;
                            }
                        }
                        break;
                    }
                    case FlightStatus.Complete:
                    {
                        break;
                    }
                    case FlightStatus.None:
                    {
                        if (i.ExpireDate <= Date.Date)
                        {
                            i.Status = FlightStatus.Expired;
                            Money -= i.ExpirePrice;
                        }
                        break;
                    }
                    case FlightStatus.Expired:
                        break;
                    case FlightStatus.WaitReturn:
                    {
                        i.Status = FlightStatus.Return;
                        if(i.Type is AirlanerType)
                            Money -= i.AssignedAirplane.ServiceCost * i.AssignedAirplane.CapacityPassenger;
                        if(i.Type is CargoType)
                            Money -= i.AssignedAirplane.ServiceCost * i.AssignedAirplane.CapacityCargo;
                        Money -= FuelPrice * i.Distance * i.AssignedAirplane.FuelCost;
                        i.Return(Date);
                        break;
                    }
                    case FlightStatus.Return:
                    {
                        if (i.EndDate <= Date)
                        {
                            i.Status = FlightStatus.None;
                            Money += i.Price;
                            i.AssignedAirplane.City = i.StartCity;
                            i.AssignedAirplane.Status = PlaneStatus.City;
                            i.AssignedAirplane.Assigned = false;
                            i.AssignedAirplane = null;
                            var t = (i.Type as AirlanerType);
                            i.StartDate = t.NextDate = Date.Date.AddDays(t.Regularity);
                            i.ExpireDate = i.StartDate.AddDays(1);
                        }
                        break;
                    }
                    default:
                        break;
                }
            }
            foreach (var i in PlayerAirplanes)
            {
                if (i.PurchaseType is Rent)
                {
                    var r = i.PurchaseType as Rent;
                    if (r.StartDate.Month != Date.Month)
                    {
                        Money -= r.Price;
                        r.NewMonth();
                    }
                }
                if (i.PurchaseType is Leasing)
                {
                    var r = i.PurchaseType as Leasing;
                    if (r.StartDate.Month != Date.Month)
                    {
                        Money -= r.Price;
                        r.NewMonth();
                    }
                }
            }
            if (Money < 0 && !Debt)
            {
                Debt = true;
                DebtStart = Date.Date;
            }
            if (Money >= 0)
                Debt = false;

            var form = (Application.OpenForms[0] as MainForm);
            // В полночь новые контракты
            if (Date.Hour == 0)
            {
                RegenerateConrtracts(10);
                form.RegenerateConrtracts();
            }
            form.UpdateContracts(Date);
            form.UpdateAirplanesInfo();
            form.UpdateInfo();
        }
        public void Wait()
        {
            Pause = true;
        }
        public void Continue()
        {
            Pause = false;
        }
    }
}
